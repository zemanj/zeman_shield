#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>

// Inicializace I2C LCD displeje
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Piny a nastavení pro LEDky
#define LED1_PIN D6
#define LED2_PIN D7

// Piny pro LED pásek
#define LED_STRIP_PIN D8
#define NUM_LEDS 8

// Piny pro fotosenzor a teplotní čidlo
#define PHOTO_PIN A0
#define TEMP_SENSOR_PIN D5

// Inicializace LED pásku
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

// Inicializace teplotního čidla DS18B20
OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensors(&oneWire);

void setup() {
  // Inicializace LCD displeje
  lcd.init();
  lcd.backlight();
  
  // Inicializace LEDek
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);

  // Inicializace LED pásku
  strip.begin();
  strip.show(); // Ujistí se, že jsou LEDky vypnuté
  
  // Inicializace teplotního senzoru
  sensors.begin();
}

void loop() {
  // Čtení hodnoty z fotosenzoru
  int photoValue = analogRead(PHOTO_PIN);
  
  // Čtení teploty z DS18B20
  sensors.requestTemperatures();
  float temperatureC = sensors.getTempCByIndex(0);

  // Zobrazení hodnot na LCD
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Light: ");
  lcd.print(photoValue);
  
  lcd.setCursor(0, 1);
  lcd.print("Temp: ");
  lcd.print(temperatureC);
  lcd.print(" C");
  
  // Zapnutí LEDek
  digitalWrite(LED1_PIN, HIGH);
  digitalWrite(LED2_PIN, HIGH);

  // Blikání LED pásku s náhodnými barvami
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(random(0, 255), random(0, 255), random(0, 255)));
  }
  strip.show();
  delay(random(100, 1000));

  // Malé zpoždění před dalším cyklem
  delay(500);
}
