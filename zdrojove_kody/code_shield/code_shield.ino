#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>

// Inicializace I2C LCD displeje
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Piny a nastavení pro LEDky
#define LED1_PIN D6
#define LED2_PIN D7

// Piny pro LED pásek
#define LED_STRIP_PIN D8
#define NUM_LEDS 8

// Piny pro fotosenzor a teplotní čidlo
#define PHOTO_PIN A0
#define TEMP_SENSOR_PIN D5

// Inicializace LED pásku
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

// Inicializace teplotního čidla DS18B20
OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(9600); // Inicializace sériové komunikace pro výpis hodnot z čidel do konzole

  // Inicializace LCD displeje
  lcd.init();
  lcd.backlight();
  
  // Inicializace LEDek
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);

  // Inicializace LED pásku
  strip.begin();
  strip.show(); // Ujistí se, že jsou LEDky vypnuté

  // Inicializace teplotního senzoru
  sensors.begin();
}

// Funkce pro testování LCD displeje - zobrazí "Ahoj"
void testLCD() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ahoj");
  delay(1000); // Zpoždění pro zobrazení zprávy na displeji
}

// Funkce pro testování fotosenzoru a teplotního čidla - vypíše hodnoty do konzole
void testSensors() {
  int photoValue = analogRead(PHOTO_PIN);
  sensors.requestTemperatures();
  float temperatureC = sensors.getTempCByIndex(0);
  
  Serial.print("Light: ");
  Serial.println(photoValue);
  
  Serial.print("Temperature: ");
  Serial.print(temperatureC);
  Serial.println(" C");
  
  delay(1000); // Zpoždění před dalším měřením
}

// Funkce pro testování LEDek - rozsvítí a zhasne LEDky
void testLEDs() {
  digitalWrite(LED1_PIN, HIGH);
  digitalWrite(LED2_PIN, HIGH);
  delay(500); // LEDky jsou rozsvícené 0,5 sekundy
  digitalWrite(LED1_PIN, LOW);
  digitalWrite(LED2_PIN, LOW);
  delay(500); // LEDky jsou zhasnuté 0,5 sekundy
}

// Funkce pro testování LED pásku - postupné rozsvěcení a zhasnutí
void testLEDStrip() {
  // Postupné rozsvěcení jedné LEDky po druhé
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(255, 0, 0)); // Nastaví LEDku na červenou barvu
    strip.show();
    delay(200); // Zpoždění mezi rozsvícením jednotlivých LEDek
  }
  
  // Zhasnutí všech LEDek
  strip.clear();
  strip.show();
  delay(500);
  
  // Rozsvícení celého LED pásku
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 255)); // Nastaví všechny LEDky na modrou barvu
  }
  strip.show();
  delay(1000); // LED pásek zůstane rozsvícený 1 sekundu
  
  // Zhasnutí všech LEDek
  strip.clear();
  strip.show();
  delay(1000); // LED pásek zůstane zhasnutý 1 sekundu
}

void loop() {
  // Odkomentuj tu funkci, kterou chceš vyzkoušet

  // testLCD();      // Testuje LCD displej - zobrazí "Ahoj"
  // testSensors();  // Testuje fotosenzor a teplotní čidlo - vypíše hodnoty do konzole
  // testLEDs();     // Testuje LEDky - rozsvítí a zhasne LEDky
  // testLEDStrip(); // Testuje LED pásek - postupné rozsvícení a zhasnutí
}
