# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | okolo 10 hodin |
| jak se mi to podařilo rozplánovat | tak na půl produkt jsem měl hotový hned, ale video a dokumentaci jsem dělal na poslední chvíli|
| design zapojení | https://gitlab.spseplzen.cz/zemanj/zeman_shield/-/blob/master/dokumentace/schema/Sn%C3%ADmek%20obrazovky%202024-09-30%20233136.png |
| proč jsem zvolil tento design | protože je cool a funkční |
| zapojení | https://gitlab.spseplzen.cz/zemanj/zeman_shield/-/blob/master/dokumentace/schema/Sn%C3%ADmek%20obrazovky%202024-09-30%20233201.png |
| z jakých součástí se zapojení skládá | Červené LED - 1 ks, Zelené LED - 1 ks, RGB LED pásek WS2812B - 1 ks, Teplotní čidlo DS18B20 - 1 ks, Fotorezistor - 1 ks, LCD displej 16x2 s I2C převodníkem - 1 ks, 10kΩ rezistor - 1 ks, 4.7kΩ rezistor - 1 ks, 200Ω rezistor - 2 ks, Prototypová deska - 1 ks, Konektory a vodiče - dle potřeby |
| realizace | https://gitlab.spseplzen.cz/zemanj/zeman_shield/-/blob/master/dokumentace/fotky/IMG20240930231840.jpg |
| nápad, v jakém produktu vše propojit dohromady| ?asi jo? |
| co se mi povedlo | vzhled |
| co se mi nepovedlo/příště bych udělal/a jinak | lépe plánovat |
| zhodnocení celé tvorby | 10/10 |
